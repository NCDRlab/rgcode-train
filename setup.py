
from setuptools import setup, find_packages

setup(
    name='rgcode-train',
    version='0.1',
    packages=find_packages(),
    entry_points='''
        [console_scripts]
        rgcode-train=rgcode_train.rgcode_train:cli
    ''',
)