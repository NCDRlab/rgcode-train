# rgcode-train
Training and testing scripts for [RGCode](https://gitlab.com/NCDRlab/rgcode)

## Set-up
rgcode-train scripts can be used in the same conda environment as RGCode. Follow the instructions on the [RGCode page](https://gitlab.com/NCDRlab/rgcode).
With the correct environment activate, to install run the following command inside of the rgcode-train folder

    (rgcode) $ pip install .

## Usage
Instruction on how to use the script is provided by running the command

    (rgcode) $ rgcode-train --help

For instructions on how to use the train command, run

    (rgcode) $ rgcode-train train --help

For instructions on how to use the test command, run

    (rgcode) $ rgcode-train test --help

## Data structure
In order for it to be recognized by rgcode-train, the data folder has to be organized in a specific structure

    DATA
    │
    └───train
    │   └───class1
    │   |   |   picture1.tif
    |   |   |   picture2.tif
    |   └───class2
    |   │   |   picture1.tif
    |   |   |   picture2.tit
    └───train_masks
    │   └───class1
    │   |   |   picture1.tif
    |   |   |   picture2.tif
    |   └───class2
    |   │   |   picture1.tif
    |   |   |   picture2.tit
    └───val_1
    │   └───class1
    │   |   |   picture1.tif
    |   |   |   picture2.tif
    |   └───class2
    |   │   |   picture1.tif
    |   |   |   picture2.tit
    └───val_masks_1
        └───class1
        |   |   picture1.tif
        |   |   picture2.tif
        └───class2
        │   |   picture1.tif
        |   |   picture2.tit

The organization in classes is optional, but a subdirectory inside the train and test folders is required.<br/>
Raw pictures are stored inside of the *train*/*val_1* folders and masks in the corresponding *masks* folders.<br/>
The name of the pictures is arbitrary, but the mask should have the same name as the corresponding raw image. It is possible to have replicate test pictrues by adding val_*number* an val_masks_*number* folders