import os
import numpy as np
import pandas as pd

from os import path
from click import progressbar
from skimage.util import img_as_float, view_as_windows, crop, montage, img_as_ubyte
from skimage.transform import rescale
from skimage.io import imread, imsave
from skimage.measure import label, regionprops
from sklearn.metrics import mean_squared_error, mean_absolute_error
from scipy import stats

from tensorflow.keras.models import load_model

from rgcode_train.lib.losses import dice_coef, dice_p_bce, true_positive_rate, euc_dist_keras



def test_model(model_path, data_path, output, args):

    if output is None:
        name_parts = path.split(model_path)
        out_folder = f"{name_parts[0]}/test_{path.splitext(name_parts[1])[0]}"
        try:
            os.mkdir(out_folder)
        except:
            print("\nFolder already exist...")
    else:
        out_folder = output

    target_size = (128, 128)

    overlap_ratio = 0.125

    batch_size = args["batch_size"]

    thresholds = [0.01, 0.025, 0.05, 0.1, 0.2, 0.4, 0.5, 0.6, 0.75, 0.8, 0.9, 0.95, 0.99]

    # Loads the original images
    val_img_files = recursive_listdir(f"{data_path}/val_1")
    val_img_files.sort()
    x_test = load_imgs(val_img_files, val_img_files)

    img_names = [path.split(name)[-1] for name in val_img_files]

    # Filter folders
    validation_folders = os.listdir(data_path)
    validation_folders = list(filter(lambda x: "val_masks_" in x, validation_folders))

    if validation_folders == []:
        print("\nTraining masks not found. Terminating...")
        return

    # Get the multiple validation masks
    mask_sets = []
    for folder in validation_folders:
        val_mask_files = recursive_listdir(f"{data_path}/{folder}")
        val_mask_files.sort()
        val_masks = load_imgs(val_mask_files, val_mask_files)
        mask_sets.append(val_masks)

    # Get the human counts and stats
    human_counts = pd.DataFrame()
    for n, mask_set in enumerate(mask_sets):
        counts = []

        for mask in mask_set:
            label_image = label(mask > 0.5)
            props = regionprops(label_image)
            true_c = len(props)
            counts.append(true_c)
        
        human_counts[f"Counter_{n + 1}"] = counts
    
    counters = human_counts.columns[:len(mask_sets)].values
    human_counts["Mean"] = human_counts[counters].mean(axis=1)
    human_counts["Median"] = human_counts[counters].median(axis=1)

    human_counts.index = img_names

    # Get prediction
    model = load_model(
        model_path,
        custom_objects={
            "dice_coef": dice_coef,
            "true_positive_rate": true_positive_rate,
            "euc_dist_keras": euc_dist_keras,
            "dice_p_bce": dice_p_bce
            }
        )
    
    preds_val = []

    print("\nPredictng the test images...")
    with progressbar(x_test, length=len(x_test)) as bar:
        for img in bar:
                block_size = target_size[0]
                overlap = int(block_size * overlap_ratio)
                step_size = block_size - overlap
                right_pad = int(overlap / 2)
                shape = np.array(img.shape)
                padded_shape = (np.ceil(shape / step_size) * step_size).astype(int)
                left_pad = padded_shape - shape + overlap + right_pad

                img = np.pad(img, pad_width=[(right_pad, left_pad[0]), (right_pad, left_pad[1])], mode='constant', constant_values=0)

                blocks = view_as_windows(img, (block_size, block_size), step=step_size)
                new_shape = (blocks.shape[0], blocks.shape[1])

                flat = np.reshape(blocks, (blocks.shape[0]*blocks.shape[1], 128, 128))

                blocks = np.reshape(blocks, (blocks.shape[0] * blocks.shape[1], blocks.shape[2], blocks.shape[3]))
                blocks = np.expand_dims(blocks, axis=-1)
                
                preds = model.predict(blocks, verbose=False)

                cropped = crop(preds.squeeze(), (
                    (0, 0),
                    (right_pad, right_pad),
                    (right_pad, right_pad),
                ))


                mon = montage(cropped, grid_shape=new_shape)
                delta_shape = mon.shape - shape
                mon = crop(mon, ((0, delta_shape[0]), (0, delta_shape[1])))

                preds_val.append(mon)
    

    # Find the threshold which gives the lowest error
    errors = []
    mean = human_counts.Mean.values

    for th in thresholds:
        true_counts = []
        pred_counts = []
        for pred, true_counts in zip(preds_val, mean):
            label_image = label(pred > th)
            props = regionprops(label_image)
            pred_c = len(props)
            pred_counts.append(pred_c)


        errors.append(np.sqrt(mean_absolute_error(mean, pred_counts)))
    
    th = thresholds[np.array(errors).argmin()]


    if args["overlay"]:
        print("\n Exporting overlays...")
        overlays = []
        for pred, orig in zip(preds_val, x_test.squeeze()):
            pred = pred.squeeze() > th
            # Creates an empty blue chennel
            blue = np.zeros(pred.shape, dtype=np.ubyte)
            # Stacks the pictures in an RGB one
            overlay = np.dstack([pred, orig, blue])
            overlays.append(overlay)


        for overlay, orig_path in zip(overlays, val_img_files):
            save_name = path.split(orig_path)[-1]
            save_name = path.join(out_folder, save_name)
            imsave(save_name, img_as_ubyte(overlay))


    
    # Get the best prediction
    true_counts = []
    pred_counts = []
    for pred in preds_val:
        label_image = label(pred > th)
        props = regionprops(label_image)
        pred_c = len(props)
        pred_counts.append(pred_c)

    human_counts["Model"] = pred_counts
    counts_savepath = f"{out_folder}/model_counts.xlsx"
    human_counts.to_excel(counts_savepath)

    # Export reports
    slope, intercept, r_value, p_value, std_err = stats.linregress(human_counts.Mean, human_counts.Model)

    stats_file = f"{out_folder}/performance.txt"

    with open(stats_file, "w") as f:
        f.write(f"optimal_threshold: {th}")
        f.write("\n\nPerformance analysis:")
        f.write(f"\nslope: {slope}")
        f.write(f"\nintercept: {intercept}")
        f.write(f"\nr_squared: {r_value}")
        f.write(f"\np_value: {p_value}")
        f.write(f"\nstd_err: {std_err}")
        f.close()
    
    print(f"Performance report saved at {stats_file}")
    print(f"Model counts saved at {counts_savepath}")
    if args["overlay"]:
        print(f"Overlays saved at {out_folder}")






def recursive_listdir(folder_path):
    file_list = []
    for root, dirs, files in os.walk(folder_path, topdown=False):
        for name in files:
                file_path = (os.path.join(root, name))
                file_list.append(file_path)
    
    return file_list


def load_imgs(roots, imgs_paths):
    imgs = []
    for full_root, img_path in zip(roots, imgs_paths):
        root = path.split(full_root)[0]
        img_path = path.split(img_path)[1]
        img_path = path.join(root, img_path)
        img = imread(img_path, as_gray=True)
        # img = rescale_intensity(img)
        img = rescale(img, 0.5)
        img = img_as_float(img)
        imgs.append(img)
    imgs = np.array(imgs)

    return imgs