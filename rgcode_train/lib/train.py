import os
from os import path
import multiprocessing

from datetime import datetime

# import tensorflow as tf
# import tensorflow.keras

# from tensorflow.keras.layers import Input
# from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
# from tensorflow.keras.optimizers import Adam, SGD
# from tensorflow.keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
# from tensorflow.keras.callbacks import TensorBoard
# import tensorflow.keras.backend as K
# from tensorflow.keras.losses import binary_crossentropy

from rgcode_train.lib.losses import dice_coef, dice_p_bce, true_positive_rate, euc_dist_keras
from rgcode_train.lib.unet import get_unet


def train_unet(params):

    import tensorflow as tf
    import tensorflow.keras

    from tensorflow.keras.layers import Input
    from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
    from tensorflow.keras.optimizers import Adam, SGD
    from tensorflow.keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
    from tensorflow.keras.callbacks import TensorBoard
    import tensorflow.keras.backend as K
    from tensorflow.keras.losses import binary_crossentropy
    

    print("Training")

    gpu_train = tf.test.is_gpu_available(cuda_only=True)

    print(f"GPU: {gpu_train}")

    print(params)

    batch_size = params["batch_size"]
    steps_per_epoch = params["steps"]
    val_split = params["validation_split"]
    epochs = params["epochs"]
    seed = params["seed"]

    data_path = params["data"]

    target_size = (128, 128)
    
    now = datetime.now()

    time = now.strftime("%Y%m%d_%H-%M-%S")

    base_name = f"rbpms_size-{target_size[0]}_split-{val_split}_batch-{batch_size}_steps-{steps_per_epoch}_epochs-{epochs}_seed-{seed}"

    model_name = time + "_" + base_name + ".h5"

    print(model_name)


    gen_args = dict(
        rescale=1./255,
        featurewise_center=False,
        featurewise_std_normalization=False,
        rotation_range=90,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        validation_split=val_split,
        fill_mode="constant",
        cval=0
    )

    datagen = ImageDataGenerator(**gen_args)

    flow_args = dict(
        target_size=target_size,
        class_mode=None,
        color_mode="grayscale",
        batch_size=batch_size,
        seed=seed,
    )

    image_generator = datagen.flow_from_directory(
        f'{data_path}/train',
        subset="training",
        **flow_args
    )

    mask_generator = datagen.flow_from_directory(
        f'{data_path}/train_masks',
        subset="training",
        **flow_args
    )

    val_image_generator = datagen.flow_from_directory(
        f'{data_path}/train',
        subset="validation",
        **flow_args
    )

    val_mask_generator = datagen.flow_from_directory(
        f'{data_path}/train_masks',
        subset="validation",
        **flow_args
    )

    train_generator = (pair for pair in zip(image_generator, mask_generator))
    val_generator = (pair for pair in zip(val_image_generator, val_mask_generator))

    input_img = Input((128, 128, 1), name='img')

    gpu_train = False

    if gpu_train:
        mirrored_strategy = tf.distribute.MirroredStrategy()
        with mirrored_strategy.scope():
            model = get_unet(input_img, n_filters=32, dropout=0.5, batchnorm=True)
            model.compile(optimizer=Adam(1e-4, decay=1e-6), loss=dice_p_bce, metrics=[dice_coef, 'binary_accuracy', true_positive_rate, euc_dist_keras])
    else:
        model = get_unet(input_img, n_filters=32, dropout=0.5, batchnorm=True)
        model.compile(optimizer=Adam(1e-4, decay=1e-6), loss=dice_p_bce, metrics=[dice_coef, 'binary_accuracy', true_positive_rate, euc_dist_keras])
    
    
    if params["transfer"] is not None:
        transfer_path = params["transfer"]
        print(f"\nTransfer learning from {path.split(transfer_path)[-1]}\n")
        model.load_weights(transfer_path)

    if params["destination"] is None:
        model_folder = os.getcwd()
    else:
        model_folder = params["destination"]

    print(model_folder)
    
    
    weight_path = f"{model_folder}/{model_name}"
    print(weight_path)


    checkpoint = ModelCheckpoint(weight_path, monitor='val_dice_coef', verbose=1, 
                                save_best_only=True, mode='max', save_weights_only = False)
    
    callbacks_list = [checkpoint]

    if params["reduce_lr"] != 0:
        reduceLROnPlat = ReduceLROnPlateau(
            monitor='val_dice_coef',
            patience=params["reduce_lr"],
            verbose=1, mode='max', epsilon=0.0001, cooldown=2, min_lr=1e-6)
        callbacks_list.append(reduceLROnPlat)
    
    early = EarlyStopping(
        monitor="val_dice_coef", 
        mode="max", 
        patience=30)

    if params["tensorboard"]:
        if os.name == 'nt':
            log_dir = f"{model_folder}\\logs\\fit\\" + model_name
        else:
            log_dir=f"{model_folder}/logs/fit/" + model_name
            
        tensorboard_callback = TensorBoard(
            log_dir=log_dir, histogram_freq=0)
        callbacks_list.append(tensorboard_callback)


    if os.name == 'nt':
        n_cores = 1
        use_mp = False
    else:
        n_cores = multiprocessing.cpu_count()
        use_mp = True

    print(f"Number of cores: {n_cores}")

    results = model.fit(train_generator,
                        steps_per_epoch=steps_per_epoch,
                        epochs=epochs,
                        callbacks=callbacks_list,
                        validation_data=val_generator,
                        validation_steps=64,
                        use_multiprocessing=use_mp,
                        workers=n_cores,
                        # max_queue_size=2048
                        )
