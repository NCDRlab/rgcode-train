import click

from rgcode_train.lib.train import train_unet
from rgcode_train.lib.test import test_model

EPOCHS = 200
STEPS = 128
BATCH_SIZE = 32
VAL_SPLIT = 0.5
REDUCE_LR = 20


@click.group()
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx, debug):

    ctx.ensure_object(dict)

    ctx.obj['DEBUG'] = debug


@cli.command()
@click.argument(
    "data",
    type=click.Path(exists=True),
    required=True)
@click.argument(
    "destination",
    type=click.Path(exists=True),
    required=False)
@click.option(
    "-i",
    "--iterations",
    required=False,
    default=1,
    help="Specify the number of models to be trained")
@click.option(
    '-t',
    '--transfer',
    type=click.Path(exists=True),
    help="Specify a model file for transfer learning")
@click.option(
    '-s',
    '--seed',
    default=False,
    help="Specify the seed to initialize the training")
@click.option(
    '-cs',
    '--constant-seed',
    "constant",
    default=False,
    help="Keep the same seed across all training iterations")
@click.option(
    '-e',
    '--epochs',
    default=EPOCHS,
    type=int,
    help=f"Specify the number of epochs (default: {EPOCHS})")
@click.option(
    '-s',
    '--steps-per-epoch',
    "steps",
    default=STEPS,
    type=int,
    help=f"Specify the number of steps per epochs (default: {STEPS})")
@click.option(
    '-b',
    '--batch-size',
    default=BATCH_SIZE,
    type=int,
    help=f"Specify the batch size (default: {BATCH_SIZE})")
@click.option(
    '-v',
    '--validation-split',
    default=VAL_SPLIT,
    type=float,
    help=f"Specify the validation split (default: {VAL_SPLIT})")
@click.option(
    '-r',
    '--reduce-lr',
    default=REDUCE_LR,
    type=int,
    help=f"Specify after how many epochs without improvement the learning rate is decreased (default: {REDUCE_LR})")
@click.option(
    "--tensorboard/--no-tensorboard", 
    default=True,
    help="Enable Tensorboard")
@click.pass_context
def train(ctx, data, destination, iterations, transfer, seed, constant, epochs, steps, batch_size, validation_split, reduce_lr, tensorboard):
    '''Training script for models based on RGCode. Run by specifying the folder containing the data. Optionally specify the output folder and the training options listed below'''
    args = locals()

    click.echo(f"Debug mode is {ctx.obj['DEBUG'] and 'on' or 'off'}")
    
    train_unet(params=args)


@cli.command()
@click.argument(
    "model",
    type=click.Path(exists=True))
@click.argument(
    "data",
    type=click.Path(exists=True))
@click.argument(
    "output",
    type=click.Path(exists=True),
    required=False)
@click.option(
    "-b",
    '--batch-size',
    default=BATCH_SIZE, 
    type=int,
    help=f"Specify the batch size (default: {BATCH_SIZE})")
@click.option(
    "-o",
    '--overlay',
    is_flag=True,
    help=f"Export image overlays of the model predictions")
@click.pass_context
def test(ctx, model, data, output, batch_size, overlay):
    '''Testing script for models based on RGCode. Run by specifying the model file and folder containing the data. Optionally specify the output folder and the training options listed below'''
    args = locals()

    click.echo(f"Debug mode is {ctx.obj['DEBUG'] and 'on' or 'off'}")
    print(args)

    test_model(model, data, output, args)





if __name__ =="__main__":
    main()